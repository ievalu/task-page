<?php
require "vendor/autoload.php";

Router::load("resources/library/router/routes.php")
  ->direct(Request::url(), Request::method());