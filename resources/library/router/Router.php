<?php

class Router {

  protected $routes = [
    "GET" => [],
    "POST" => [],
    "PATCH" => [],
    "DELETE" => []
  ];

  public static function load($file){
    $router = new static;
    require $file;
    return $router;
  }

  public function define($routes){
    return $this->routes = $routes;
  }

  public function get($url, $controller){
    return $this->routes["GET"][$url] = $controller;
  }

  public function post($url, $controller){
    return $this->routes["POST"][$url] = $controller;
  }

  public function patch($url, $controller){
    return $this->routes["PATCH"][$url] = $controller;
  }

  public function delete($url, $controller){
    return $this->routes["DELETE"][$url] = $controller;
  }

  public function direct($url, $requestType){
    if(array_key_exists($url, $this->routes[$requestType])){
      return $this->callAction(...explode("@", $this->routes[$requestType][$url]));
    }
    throw new Exception("No route defined for this URL");
  }

  protected function callAction($controller, $action){
    if(! method_exists($controller, $action)){
      throw new Exception(
        "{$controller} does not respond to the {$action} action"
      );
    }
    return (new $controller("resources/library/database/bootstrap.php"))->$action();
  }
}