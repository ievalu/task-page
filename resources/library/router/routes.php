<?php

$router->get("", "PageController@home");
$router->get("old", "PageController@old");
$router->get("upcoming", "PageController@upcoming");
// move everything below to TaskController, with decent naming
$router->get("task", "TaskController@get"); // Task/get
$router->get("task/new", "TaskController@get_new_task"); // Task/new/get
$router->post("task", "TaskController@post"); // Task/post
$router->patch("task", "TaskController@patch"); // Task/patch
$router->delete("task", "TaskController@delete"); // Task/delete