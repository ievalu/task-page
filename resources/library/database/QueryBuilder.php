<?php

class QueryBuilder{

  protected $pdo;

  public function __construct($pdo){
    $this->pdo = $pdo;
  }

  public function selectAll($table){
    $statement = $this->pdo->prepare("select * from {$table}");
    $statement->execute();
    return $statement->fetchAll(PDO::FETCH_CLASS);
  }

  public function selectById($table, $id){
    $statement = $this->pdo->prepare("select * from {$table} where TaskID={$id}");
    $statement->execute();
    return $statement->fetchAll(PDO::FETCH_CLASS);
  }

  public function insert($table, $title, $description, $date){
    $statement = $this->pdo->prepare("insert into {$table} (Title, Description, Date) values ('{$title}', '{$description}', '{$date}')");
    $statement->execute();
  }

  public function update($table, $set, $where){
    $new_set = [];
    foreach($set as $col=>$value){
      $new_set[] = "{$col} = {$value}";
    }
    $set_string = implode(",", $new_set);
    $new_where = [];
    foreach($where as $col=>$value){
      $new_where[] = "{$col} = {$value}";
    }
    $where_string = implode(" AND ", $new_where);
    $statement = $this->pdo->prepare("UPDATE {$table} SET {$set_string} WHERE {$where_string}");
    $statement->execute();
  }

  public function delete($table, $id){
    $statement = $this->pdo->prepare("DELETE FROM task WHERE TaskID = {$id}");
    $statement->execute();
  }

  public function select_with_limitter($table, $offset, $no_of_records_per_page, $upcoming=false){
    date_default_timezone_set("Europe/Vilnius");
    $today = date("Y-m-d h:i:sa");
    $statement = $this->pdo->prepare("SELECT * FROM {$table} WHERE Date".($upcoming ? ">=" : "<")."'{$today}' LIMIT {$offset}, {$no_of_records_per_page}");
    $statement->execute();
    return $statement->fetchAll(PDO::FETCH_CLASS);
  }

  public function count_rows($table, $upcoming=true){
    date_default_timezone_set("Europe/Vilnius");
    $today = date("Y-m-d h:i:sa");
    $statement = $this->pdo->prepare("SELECT COUNT(*) FROM {$table} WHERE Date".($upcoming ? ">=" : "<")."'{$today}'");
    $statement->execute();
    return $statement->fetchColumn();
  }
}