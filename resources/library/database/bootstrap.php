<?php

require "vendor/autoload.php";
$config = require "resources/config.php";

return  new QueryBuilder(Connection::make($config["database"]));