<html>
  <head>
    <link rel="stylesheet" type="text/css" href="../public-html/css/description.css">
    <link href="https://fonts.googleapis.com/css?family=Didact+Gothic" rel="stylesheet">
  </head>
  <body>
    <h1 class="title">
      <?= $task->title ?>
    </h1>
    <div class="description">
      <?= $task->description ?>
    </div>
  </body>
</html>