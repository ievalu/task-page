<html>
  <head>
  <link rel="stylesheet" type="text/css" href="../public-html/css/countSelector.css">
  <link href="https://fonts.googleapis.com/css?family=Didact+Gothic" rel="stylesheet">
  </head>
  <body>
    <div class="dropdown">
      <button class="btn btn-secondary dropdown-toggle dropdown-button" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <?= $no_of_records ?>
      </button>
      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
        <?php for($i=5;$i<=55;$i += 10): ?>
        <a class="dropdown-item" href="?page=<?= isset($_GET['page']) ? $_GET['page'] : 1 ?>&count=<?= $i ?>"><?= $i ?></a>
        <?php endfor; ?>
      </div>
    </div>
  </body>
</html>