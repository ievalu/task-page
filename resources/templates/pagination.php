<html>
  <head>
  <link rel="stylesheet" type="text/css" href="../public-html/css/pagination.css">
  <link href="https://fonts.googleapis.com/css?family=Didact+Gothic" rel="stylesheet">
  </head>
  <body>
    <nav aria-label="Search tasks">
      <ul class="pagination">
        <?php for($i=1;$i<=$total_pages;$i++): ?>
          <li class="page-item"><a class="page-link" href="?page=<?= $i ?>"><?= $i ?></a></li>
        <?php endfor; ?>
      </ul>
    </nav>
  </body>
</html>