<html>
  <head>
  <link rel="stylesheet" type="text/css" href="../public-html/css/nav.css">
  <link href="https://fonts.googleapis.com/css?family=Didact+Gothic" rel="stylesheet">
  </head>
  <body>
  <nav class="nav">
    <a class="nav-link" href="/">Home</a>
    <a class="nav-link" href="/upcoming">Upcoming</a>
    <a class="nav-link" href="/old">Old</a>
    <a class="nav-link" href="/task/new">Add new task</a>
  </nav>
  </body>
</html>