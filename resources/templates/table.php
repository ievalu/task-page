<html>
  <head>
    <link rel="stylesheet" type="text/css" href="../public-html/css/table.css">
    <link href="https://fonts.googleapis.com/css?family=Didact+Gothic" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
    <script src="../public-html/js/update-task.js"></script>
  </head>
  <body>
    <table class="table">
      <thead>
        <tr>
          <th scope="col">Title</th>
          <th scope="col">Date</th>
          <th scope="col">Done</th>
          <th scope="col">Remove</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach($tasks as $task): ?>
        <tr>
          <td><a href="/task?id=<?= $task->id ?>"><?= $task->title ?></a></td>
          <td><?= $task->date ?></td>
          <td>
            <span class="completed" onclick="updateTask(<?= $task-> id ?>, '<?= $task->completed ?>')"><?= $task->completed ?></span>
          </td>
          <td><img class="remove" src="../../public-html/img/remove-cross.jpg" alt="Remove" onclick="removeTask(<?= $task-> id ?>)"></td>
        </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </body>
</html>