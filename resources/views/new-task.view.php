<?php require "vendor/autoload.php" ?>
<html>
 <head>
  <title>Tasks-add-new</title>
  <link rel="stylesheet" type="text/css" href="../public-html/css/task.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
 </head>
 <body>
  <?php require "resources/templates/nav.php"; ?>
  <h1 class="page-title">Add new task</h1>
  <form method="POST" action="/task">
    <div class="input">
      <label>Title</label>
      <input type="text" size="28" name="title"/>
    </div>
    <div class="input">
      <label>Description</label>
      <textarea cols="31" rows="4" name="description"></textarea>
    </div>
    <div class="input">
      <label>Deadline</label>
      <input type="datetime-local" step="1" name="date"/>
    </div>
    <button type="submit" class="input">Add</button>
  </form>
 </body>
</html>