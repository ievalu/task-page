<?php

require "vendor/autoload.php";


class PageController{

  protected $query;

  public function __construct($query){
    $this->query = require $query;
  }

  protected function mapToTask($tasks){
    $tasks = array_map(function($task){
      $t = new Task();
      $t->id = $task->TaskID;
      $t->title = $task->Title;
      $t->description = $task->Description;
      $t->date = $task->Date;
      $t->completed = $task->Completed;
      return $t;
    }, $tasks);

    return $tasks;
  }

  protected function completedToBool($tasks){
    foreach($tasks as $task){
      if($task->completed == 1){
        $task->completed = "Yes";
      }
      else{
        $task->completed = "No";
      }
    }
    return $tasks;
  }

  protected function get_offset_number($no_of_records){
    if (isset($_GET['page'])) {
      $pageno = $_GET['page'];
    } else {
      $pageno = 1;
    }
    $offset = ($pageno-1) * $no_of_records;

    return $offset;
  }

  public function home(){
    require "resources/views/index.view.php";
  }

  public function upcoming(){

    if (isset($_GET['count'])) {
      $no_of_records = $_GET['count'];
    } else {
      $no_of_records = 7;
    }
    $offset = $this->get_offset_number($no_of_records);
    $total_records = $this->query->count_rows("task");
    $total_pages = ceil($total_records/$no_of_records);

    $tasks = $this->query->select_with_limitter("task", $offset, $no_of_records, true);

    $tasks = $this->mapToTask($tasks);
    $tasks = $this->completedToBool($tasks);

    require "resources/views/upcoming.view.php";
  }

  public function old(){

    if (isset($_GET['count'])) {
      $no_of_records = $_GET['count'];
    } else {
      $no_of_records = 7;
    }
    $offset = $this->get_offset_number($no_of_records);
    $total_records = $this->query->count_rows("task", false);
    $total_pages = ceil($total_records/$no_of_records);

    $tasks = $this->query->select_with_limitter("task", $offset, $no_of_records);

    $tasks = $this->mapToTask($tasks);
    $tasks = $this->completedToBool($tasks);

    require "resources/views/old.view.php";
  }
}