<?php

require "vendor/autoload.php";


class TaskController{

  protected $query;

  public function __construct($query){
    $this->query = require $query;
  }

  protected function mapToTask($tasks){
    $tasks = array_map(function($task){
      $t = new Task();
      $t->id = $task->TaskID;
      $t->title = $task->Title;
      $t->description = $task->Description;
      $t->date = $task->Date;
      $t->completed = $task->Completed;
      return $t;
    }, $tasks);

    return $tasks;
  }

  public function get(){
    $id = explode("=" ,$_SERVER['QUERY_STRING'])[1];

    $tasks = $this->query->selectById("task", $id);

    $task = $this->mapToTask($tasks)[0];

    require "resources/views/description.view.php";
  }

  public function get_new_task(){

    require "resources/views/new-task.view.php";
  }

  public function post(){
    $this->query->insert("task", $_POST["title"], $_POST["description"], $_POST["date"]);
    header("Location: /task/new");
  }

  public function patch(){
    parse_str(file_get_contents('php://input'), $_PATCH);
    $this->query->update("task", array("Completed" => $_PATCH["newValue"]), array("TaskID" => $_PATCH["id"]));
  }

  public function delete(){
    parse_str(file_get_contents('php://input'), $_DELETE);
    $this->query->delete("task", $_DELETE["id"]);
  }
}