function updateTask(id, label){
  $.ajax({
    url : 'http://localhost:8888/task',
    data : {id: id, newValue: label === 'Yes' ? 0 : 1},
    type : 'PATCH',
    contentType : 'application/json'
  })
  .then(() => {
    location.reload()
  })
}

function removeTask(id){
  if(window.confirm("Are you sure?")){
    $.ajax({
      url : 'http://localhost:8888/task',
      data : {id: id},
      type : 'DELETE',
      contentType : 'application/json'
    })
    .then(() => {
      location.reload()
    })
  }
}
